import log from "@ajar/marker";
import net from "net";
import fs from "fs";

const server = net.createServer();

server.on("connection", (socket) => {
    log.cyan("backup-server connected!");

    socket.on("data", async (buffer) => {
        const { data, file_name } = JSON.parse(buffer.toString().trim());
        log.yellow("-> incoming:", file_name);

        // create write stream
        const writeStream = fs.createWriteStream(`./${file_name}`, {
            encoding: "utf-8",
            flags: "a",
        });
        writeStream.write(data);
    });

    socket.on("end", () => {
        log.yellow("disconnected from storage-server");
    });
});

server.on("error", (err) => log.error(err));

server.listen(8124, () => log.cyan("storage-server is up 🚀🚀"));
